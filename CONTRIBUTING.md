# Contribution Guidelines
Please follow the instructions below to make a contribution.

## Table of Content

- [Adding to the list](#adding-to-the-list)
- [Removing from the List](#removing-from-the-list)
- [Contributors](#contributors)

## Adding to the List

- Please add the content to the `README.md` file and make sure that the edited list is in alphabetical order.
- Use the following format: `[GAME](LINK) - DESCRIPTION.`
- New categories, or improvements to the existing categorization are welcome.
- End all descriptions with a full stop/period.
- Submit a pull request.

## Removing from the List

Typical reasons for deleting project:

- Deprecated
- Lacks license

## Contributors

Your contributions are always welcome!  Thank you for your suggestions! :smiley:# Contribution Guidelines
