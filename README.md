<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/Text" property="dct:title" rel="dct:type"> Games-list</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/gamearians/" property="cc:attributionName" rel="cc:attributionURL">Gamearians</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://gitlab.com/gamearians/Games-list/" rel="dct:source">https://gitlab.com/gamearians/Games-list/</a>.

# [Games on GitLab](https://gitlab.com/gamearians/Games-list) <img src="http://gitlab-org.gitlab.io/gitlab-svgs/dist/illustrations/gitlab_logo.svg" height="42" alt="Fox head" title="Games on GitLab">
Below is a list of game sources that can be found on GitLab

#### Contributing
If you'd like to add a repository to the list, please fork this repository and submit a pull request ([click here to edit this file from gitlab](https://gitlab.com/gamearians/Games-list/blob/master/README.md)) or [create an Issue](https://gamearians.gitlab.io/Games-list/issues).
Help: [MarkDown Help](https://gitlab.com/help/user/markdown.md), [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

-------

 - [0 A.D.](https://gitlab.com/0ad/0ad) - (pronounced "zero ey-dee") is a free, open-source, cross-platform, real-time strategy game of ancient warfare.
 - [1oom](https://gitlab.com/KilgoreTroutMaskReplicant/1oom) - Master of Orion (1993) game engine recreation.
 - [Anonymine](https://gitlab.com/oskog97/anonymine) - Terminal minesweeper with no guessing required and a couple of unique features.
 - [EXO_encounter 667](https://gitlab.com/technomancy/exo-encounter-667) - You play as an unmanned expedition to the remote exoplanet Gliese 667 Cc which crashes near ruins of unknown origin.
 - [FileWalker](https://gitlab.com/VsevolodKM/FileWalker) - It is like normal file manager, but it is also a game.
 - [FreedroidRPG](https://gitlab.com/freedroid/freedroid-src) - FreedroidRPG is an original isometric 3D role playing game taking place in the future, on Earth. It features action and dialogs.
 - [freesiege](https://gitlab.com/LibreGames/freesiege) - An intense tetris-like wargame.
 - [Jump'n Bump](https://gitlab.com/LibreGames/jumpnbump) - play cute bunnies jumping on each other's heads.
 - [lugaru](https://gitlab.com/osslugaru/lugaru) - Lugaru HD, free and open source ninja rabbit fighting game.
 - [minetest](https://gitlab.com/minetest/minetest) - Minetest, an open source infinite-world block sandbox game with survival and crafting.
 - [OpenMW](https://gitlab.com/OpenMW/openmw) -  an open-source open-world RPG game engine that supports playing Morrowind.
 - [Rocket Guardian](https://gitlab.com/atorresm/rocket-guardian) - Rocket Guardian is a android game about a guardian that must protect a city from the falling zombies.
 - [solaria](https://gitlab.com/atorresm/solaria) - Android game about maintaining a garden in an relaxing ambient.
 - [SuperTuxParty](https://gitlab.com/SuperTuxParty/SuperTuxParty) - An open-source party game inspired by games such as Mario Party.
 - [Starun](https://gitlab.com/atorresm/Starun) - A simple game about a cute star's travel.
 - [void](https://gitlab.com/tek256/void) - A cross platform 2d adventure game.
 - [very-hungry-penguins](https://gitlab.com/softwa/very-hungry-penguins) - Hungry penguins game.
 - [Vegan on a Desert Island](https://gitlab.com/voadi/voadi) - 2D adventure game about being a vegan on a desert island.
 - [voxelands](https://gitlab.com/voxelands/voxelands) - Main development tree for the 3D voxel world game Voxelands.
 - [zsdx](https://gitlab.com/solarus-games/zsdx) - Zelda fangame created with the Solarus engine.